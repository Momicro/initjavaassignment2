package com.SchMo.assignment2.models;

public class Country {

    private String countryName;
    private String countryCustomerCount;

    public Country(String countryName, String countryCustomerCount) {

        this.countryName = countryName;
        this.countryCustomerCount = countryCustomerCount;
    }

    public Country() {
    }


    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getCountryCustomerCount() {
        return countryCustomerCount;
    }

    public void setCountryCustomerCount(String countryCustomerCount) {
        this.countryCustomerCount = countryCustomerCount;
    }
}
