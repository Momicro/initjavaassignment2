package com.SchMo.assignment2.controllers;

import com.SchMo.assignment2.models.Country;
import com.SchMo.assignment2.models.Customer;
//import com.SchMo.assignment2.models.Spender;
import com.SchMo.assignment2.repositories.CustomerRepositoryImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;

//@RestController
@Controller
@RequestMapping(path = "/")
public class CustomerController {

    private final CustomerRepositoryImpl customerRepository;

    @Autowired
    public CustomerController( CustomerRepositoryImpl customerRepository) {
        this.customerRepository = customerRepository;
    }
    //CustomerRepositoryImpl customerRepository = new CustomerRepositoryImpl();

    @GetMapping("api/customers")
    @ResponseBody
    public ArrayList<Customer> getAllCustomer(){

        return customerRepository.getAllCustomers();
    }

    @GetMapping("api/customers/{id}")
    @ResponseBody
    public Customer getCustomerByPathId(@PathVariable String id) {
        return customerRepository.getCustomerById(id);
    }

    @GetMapping("api/searchByLastName/{lastName}")
    @ResponseBody
    public Customer getCustomerByPathlastName(@PathVariable String lastName) {
        return customerRepository.getCustomerBylastName(lastName);
    }

    @GetMapping("api/customerSet/{start}to{end}")
    @ResponseBody
    public ArrayList<Customer> getCustomerSet(@PathVariable String start, @PathVariable String end) {
        System.out.println(start+" to "+end);
        return customerRepository.getCustomerSet(start, end);
    }

    @GetMapping("api/customerRand")
    @ResponseBody
    public ArrayList<Customer> getCustomerRand() {
        return customerRepository.getCustomerRand();
    }

    //@PostMapping("api/addNew")
    @RequestMapping(value = "api/addNew")
    @ResponseBody
    public Boolean addNewCustomer() {
        customerRepository.addNewCustomer();
        return false;
    }

    //@PutMapping("api/updateCustomers={id}")
    @RequestMapping(value = "api/updateCustomer={id}")
    @ResponseBody
    //public Boolean updateExistingCustomer(@PathVariable String id, @RequestBody Customer customer){
        public Boolean updateExistingCustomer(@RequestBody Customer customer, @PathVariable String id){
        customerRepository.updateExistingCustomer(customer, id);
        return false;
    }

    @RequestMapping(value = "api/deleteCustomer={email}")
    @ResponseBody
    public Boolean deleteCustomer(@PathVariable String email) {
        customerRepository.deleteCustomer(email);
        return false;
    }

//=================================================================================
    @RequestMapping("api/countries")
    @ResponseBody
    public ArrayList<Country> showCountryRanking(){
        return customerRepository.getCountryRanking();
    }

    //@RequestMapping("api/spenders")
    //@ResponseBody
    //public ArrayList<Spender> showHighestSpenders(){
       // return customerRepository.getHighestSpenders();
   // }

    @RequestMapping("api/customers/{id}/popular")
    @ResponseBody
    public ArrayList<String> showCustomersInvoices(@PathVariable String id) {
        return customerRepository.getInvoices(id);
    }

    @RequestMapping("api/customers/{id}/popular/genres")
    @ResponseBody
    public ArrayList<String> showCustomersTracklist(@PathVariable String id) {
        ArrayList<String> invoices = customerRepository.getInvoices(id);
        ArrayList<String> genreList = new ArrayList<>();
        ArrayList<String> mostPopularGenre = new ArrayList<>();
        ArrayList<String> mostPopularGenreNames = new ArrayList<>();
        int frequency = 0;
        for (String i : invoices) {
            genreList.add(customerRepository.getGenreId(customerRepository.getTrackId(i)));
        }

        for (int i =0 ; i<genreList.size(); i++) {
            //if the current elements frequency is higher than the last dedicated frequency-maximum AND the
            if ( Collections.frequency(genreList, genreList.get(i))>frequency) {
                frequency = Collections.frequency(genreList, genreList.get(i));//set new frequency maximum
                mostPopularGenre = new ArrayList<>(); //reset the Array with the id of the current maximum
                mostPopularGenre.add(genreList.get(i));
            } else if(Collections.frequency(genreList, genreList.get(i))==frequency
                    && !genreList.get(i).equals(mostPopularGenre.get(0))  // if the Id is not equal to the first two elements it should be added
                    && !genreList.get(i).equals(mostPopularGenre.get(1)) ){
                mostPopularGenre.add(genreList.get(i));
            }
        }

        for (String i:mostPopularGenre) {
            mostPopularGenreNames.add(customerRepository.getGenreName(i));
        }

        return mostPopularGenreNames;
    }


    //===================== Thymeleaf ================================

    @RequestMapping(value = "/", method = RequestMethod.GET)
    @ResponseBody
    public String home(Model model) {
        ArrayList<Customer> customers = customerRepository.getAllCustomers();
        model.addAttribute("customers", customers);
        Customer customer = getCustomerByPathlastName("Hansen");
        model.addAttribute("customer", customer);
        return "home";
    }

    @RequestMapping(value = "/searchResult", method = RequestMethod.GET)
    @ResponseBody
    public String searchResult(@ModelAttribute Customer customer, Model model) {
        //Customer customer = cu
        Customer result = customerRepository.getCustomerBylastName(customer.getLastName());
        model.addAttribute("result", result);
        return "searchResult";
    }

    @RequestMapping(value = "/searchResult", method = RequestMethod.POST)
    @ResponseBody
    public String searchResult(@ModelAttribute Customer customer, BindingResult errors, Model model) {
        Customer result = customerRepository.getCustomerBylastName(customer.getLastName());
        model.addAttribute("result", result);
        return "searchResult";
    }
}
