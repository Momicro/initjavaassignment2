package com.SchMo.assignment2.repositories;

import com.SchMo.assignment2.models.Country;
import com.SchMo.assignment2.models.Customer;
import com.SchMo.assignment2.utils.ConnectionHelper;
import com.SchMo.assignment2.utils.ConsoleLogger;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.Optional;

@Service
public class CustomerRepositoryImpl {

    //Setting up the connection object we need.
    private String URL = ConnectionHelper.CONNECTION_URL;
    private Connection conn = null;
    private ConsoleLogger logger = new ConsoleLogger();

    public ArrayList<Customer> getAllCustomers() {
        ArrayList<Customer>customers = new ArrayList<Customer>();
        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customerId, firstName, lastName, country, postalCode, phone, email FROM customer");

            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getString("customerId"),
                                resultSet.getString("firstName"),
                                resultSet.getString("lastName"),
                                resultSet.getString("country"),
                                resultSet.getString("postalCode"),
                                resultSet.getString("phone"),
                                resultSet.getString("email")
                        ));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return customers;
    }

    public Customer getCustomerById(String id) {
        Customer customer = null;
        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customerId, firstName, lastName, country, postalCode, phone, email FROM customer WHERE customerId=?");
            preparedStatement.setString(1,id);

            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            customer = new Customer(
                    resultSet.getString("customerId"),
                    resultSet.getString("firstName"),
                    resultSet.getString("lastName"),
                    resultSet.getString("country"),
                    resultSet.getString("postalCode"),
                    resultSet.getString("phone"),
                    resultSet.getString("email")
            );

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return customer;
    }

    public Customer getCustomerBylastName(String lastName) {
        Customer customer = null;
        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customerId, firstName, lastName, country, postalCode, phone, email FROM customer WHERE lastName=?");
            preparedStatement.setString(1,lastName);

            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            customer = new Customer(
                    resultSet.getString("customerId"),
                    resultSet.getString("firstName"),
                    resultSet.getString("lastName"),
                    resultSet.getString("country"),
                    resultSet.getString("postalCode"),
                    resultSet.getString("phone"),
                    resultSet.getString("email")
            );

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return customer;
    }

    public ArrayList<Customer> getCustomerRand() {
        ArrayList<Customer>customers = new ArrayList<Customer>();
        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customerId, firstName, lastName, country, postalCode, phone, email FROM customer ORDER BY RAND() LIMIT 5");

            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getString("customerId"),
                                resultSet.getString("firstName"),
                                resultSet.getString("lastName"),
                                resultSet.getString("country"),
                                resultSet.getString("postalCode"),
                                resultSet.getString("phone"),
                                resultSet.getString("email")
                        ));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return customers;
    }

    public ArrayList<Customer> getCustomerSet(String start, String end) {
        //Customer customer = null;
        ArrayList<Customer>customers = new ArrayList<Customer>();

        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customerId, firstName, lastName, country, postalCode, phone, email FROM customer ORDER BY customerId LIMIT ? OFFSET ?");
            preparedStatement.setString(1, start);
            preparedStatement.setString(2,end);

            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                customers.add(
                        new Customer(
                                resultSet.getString("customerId"),
                                resultSet.getString("firstName"),
                                resultSet.getString("lastName"),
                                resultSet.getString("country"),
                                resultSet.getString("postalCode"),
                                resultSet.getString("phone"),
                                resultSet.getString("email")
                        ));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return customers;
    }

    public Boolean addNewCustomer() {
        //Customer customer = null;
        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("INSERT INTO customer (customerId, firstName, lastName, country, postalCode, phone, email)" +
                            "VALUES (?, ?, ?, ?, ?, ?, ?)");

            preparedStatement.setString(1,"60");
            preparedStatement.setString(2, "Ingo");
            preparedStatement.setString(3,"Schmalstieg");
            preparedStatement.setString(4,"Germany");
            preparedStatement.setString(5,"12345");
            preparedStatement.setString(6,"013747846645");
            preparedStatement.setString(7,"ingo@noweb.com");

            //Execute Query
            //preparedStatement.executeQuery();
            preparedStatement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }

    public Boolean updateExistingCustomer(Customer updatedCustomer,String id) {

        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("UPDATE customer\nSET firstName=?, lastName=?, country=?, postalCode=?, phone=?, email=? \nWHERE customerId =?");

            preparedStatement.setString(1,updatedCustomer.getFirstName());
            preparedStatement.setString(2, updatedCustomer.getLastName());
            preparedStatement.setString(3,updatedCustomer.getCountry());
            preparedStatement.setString(4,updatedCustomer.getPostalCode());
            preparedStatement.setString(5,updatedCustomer.getPhone());
            preparedStatement.setString(6,updatedCustomer.getEmail());
            preparedStatement.setString(7,id);

            //Execute Update
            preparedStatement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }


    public Boolean deleteCustomer(String email) {
        //Customer customer = null;
        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("DELETE FROM customer WHERE email=?");

            preparedStatement.setString(1,email);

            //Execute Query
            //preparedStatement.executeQuery();
            preparedStatement.executeUpdate();

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
        return false;
    }


    public ArrayList<Country> getCountryRanking(){

        ArrayList<Country>countries = new ArrayList<>();
        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT country, COUNT(CustomerId),country FROM customer GROUP BY country ORDER BY COUNT(CustomerId) DESC");

            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                countries.add(
                        new Country(
                                resultSet.getString("country"),
                                resultSet.getString("COUNT(CustomerId)")
                        ));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        return countries;
    }
/*
    public ArrayList<Spender> getHighestSpenders(){

        ArrayList<Spender>highSpenders = new ArrayList<>();
        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT customerId, COUNT(invoiceId) FROM invoice GROUP BY customerId ORDER BY COUNT(invoiceId) DESC");

            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                highSpenders.add(
                        new Spender(
                                resultSet.getString("customerId"), //FirstName + LastName would need another request
                                resultSet.getString("COUNT(invoiceId)")
                        ));
            }

        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        return highSpenders;
    }
*/
    public ArrayList<String> getInvoices(String id){

        ArrayList<String> invoiceList = new ArrayList<>();

        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT invoiceId FROM invoice WHERE customerId = ?");
            preparedStatement.setString(1,id);


            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                invoiceList.add(
                        resultSet.getString("invoiceId")
                );
            }



        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        return invoiceList;
    }

    public String getTrackId(String invoiceId) {
        String trackId = null;

        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT trackId FROM invoiceline WHERE invoiceId = ?");
            preparedStatement.setString(1,invoiceId);


            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            trackId = resultSet.getString("trackId");



        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        return trackId;
    }

    public String getGenreId(String trackId){
        String genreId = null;

        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT genreId FROM track WHERE trackId = ?");
            preparedStatement.setString(1,trackId);


            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            genreId = resultSet.getString("genreId");


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        return genreId;
    }

    public String getGenreName(String genreId){
        String genreName = null;

        try{
            //Connect to DB
            conn = DriverManager.getConnection(URL);
            logger.log("Connection to SQLite has been established.");

            //Make SQL query
            PreparedStatement preparedStatement =
                    conn.prepareStatement("SELECT name FROM genre WHERE genreId = ?");
            preparedStatement.setString(1,genreId);


            //Execute Query
            ResultSet resultSet = preparedStatement.executeQuery();

            genreName= resultSet.getString("name");


        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try{
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        return genreName;
    }
    //Request for the heard tracks grouped by specific user --> Request in invoice_item to get the Trackid --> Request in Tracks to get the Genre --> count genres --> display tracks

}

